A basic example of a Behavior Tree in TypeScript

### Usage

Assuming you have the last version of TypeScript installed, type

```
tsc
```

and then

```
node .\app\btexample
```