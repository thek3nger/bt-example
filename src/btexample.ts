/**
 * Behavior Tree minimal example.
 */

// Here we define the general interface for a behavior tree node.
interface BTNode {
    children: BTNode[];
    execute(): boolean; // In general, execute returns the node state that is more than a boolean.
}
// We can provide a method to add a child. 
// For now, we just access directly the children list.

// Behavior Tree main class.
class BehaviorTree {
    root: BTNode;
    constructor() {
        this.root = null;
    }

    execute() : boolean {
        return this.root.execute();
    }
}

// Sequence: return a failure at the first failure.
class Sequence implements BTNode {

    children: BTNode[];

    constructor() {
        this.children = [];
    }

    execute() : boolean {
        for (let c of this.children) {
            let state = c.execute();
            if (!state) return false;
        }
        return true;
    }
}

// Select: return a success at the first success.
class Select implements BTNode {
    children: BTNode[];

    constructor() {
        this.children = [];
    }

    execute() : boolean {
        for (let c of this.children) {
            let state = c.execute();
            if (state) return true;
        }
        return false;
    }
}

// A base class for a task (node without children).
abstract class Task implements BTNode {
    children: BTNode[];

    constructor() {
        this.children = [];
    }

    abstract execute();
}

// BlackBoard.
// BlackBoards are usually more complicated than a global variable.
// But for simplicity sake...
let BB = { world: { door_locked: true, distance: 3 }};

// Tasks
// We implement three basic tasks for a bot that is trying to open a door.
// Note that tasks can write and read from the BlackBoard.
class UnlockDoor extends Task {
    constructor() { super() }

    execute() {
        console.log("Trying to unlock the door.");
        if (BB.world.distance > 0) {
            console.log("I'm too far away!");
            return false;
        }
        BB.world.door_locked = false;
        console.log("Door is now unlocked!");
        return true;
    }
}

class GoNear extends Task {
    constructor() { super() }

    execute() {
        console.log("I go near!");
        BB.world.distance--;
        return true;
    }
}

class OpenDoor extends Task {
    constructor() { super() }

    execute() {
        console.log("Trying to open the door.");
        if (BB.world.door_locked) {
            console.log("Door is locked!");
            return false;
        }
        console.log("I open the door!");
        return true;
    }
}

// The main function. We run the BT four times. Each time the BB is updated.
// After 4 steps, the bot is able to open the door.
function main() {
    let BT = new BehaviorTree();
    BT.root = new Select();
    let unlock = new UnlockDoor();
    let gonear = new GoNear();
    let opendoor = new OpenDoor();
    let seq = new Sequence();
    seq.children = [gonear, unlock];
    BT.root.children = [opendoor, seq];

    console.log("-- Step 1");
    BT.execute();
    console.log("-- Step 2");
    BT.execute();
    console.log("-- Step 3");
    BT.execute();
    console.log("-- Step 4");
    BT.execute();
}

main();